

------------- Dossier Modélisation ------------- 

---> Charger les sources de données dans  Modélisation --> Inputs

Lien de téléchargement des sources de données : https://www.kaggle.com/c/home-credit-default-risk/data

Les tables les plus importantes sont : 

- application_train.csv
- application_test.csv
- bureau.csv
- bureau_balance.csv




------------- Dossier Dashboard --------------------

---> Pour déployer le Dashboard, copier et coller le fichier Application_train.csv  dans le dossier que le fichier app.py.



------------- Dossier API -----------------
1 - Pour déployer l'API, copier le fichier finalized_model_fastapi.pkl qui se trouve dans le dossier Modélisation. 
2- Coller ce fichier dans le Dossier API



------------- Liens vers les applications ------------- 
Dépôt bitbucket : https://Ferk14@bitbucket.org/Ferk14/p7_02_dossier.git

----> Lien du Dashboard : https://pretadepenser-dashboard.herokuapp.com/
----> Lien documentation de l'API : https://fastapiproject7.herokuapp.com/docs

